# Generated with MetaR, http://metaR.campagnelab.org, from script "DownloadExAc" on Wed Jun 29 12:09:17 EDT 2016
#This script assumes that you have configured the ALLOGENOMICS path variable (in MPS preferences)
#to point to where this project has been cloned.
 
#Note that you must do git clone git@github.com:macarthur-lab/exac_2015.git and adjust the directory
#where you cloned exac_2015 in the line below:
setwd("/Users/fac2003/MPSProjects/git/exac_2015")

 
if(!require("plyr")
){install.packages("plyr",repos="http://cran.us.r-project.org")

library("plyr")
}
if(!require("dplyr")
){install.packages("dplyr",repos="http://cran.us.r-project.org")

library("dplyr")
}
source("exac_constants.R")

exac=load_exac_data()

use_data=subset(exac,use)

reduced<-select(use_data,chrom,pos,id,af_global,af_popmax,maf_global)

write.table(reduced,"/Users/fac2003/MPSProjects/git/AllogenomicsAnalyses/results/ExAc-reduced.tsv",sep="\t",row.names=FALSE,quote=FALSE)

 