# README #

This repository provides the code to reproduce the analysis presented in the allogenomics manuscript as well as deidentifed aggregate data. The code is written with [MetaR](http://metaR.campagnelab.org), a simple language for data analysis that simplifies data analysis with R for biologists. The data can be found under the data directory.

See the following preprints for more details. 


Exome Sequencing and Prediction of Long-Term Kidney Allograft Function. Laurent Mesnard, Thangamani Muthukumar, Maren Burbach, Carol Li, Huimin Shang, Darshana Dadhania, John R Lee, Vijay K Sharma, Jenny Xiang, Caroline Suberbielle, Maryvonnick Carmagnat, Nacera Ouali, Eric Rondeau, John Friedewald, Michael Abecassis, Manikkam Suthanthiran, Fabien Campagne doi: [http://dx.doi.org/10.1101/015651](http://dx.doi.org/10.1101/015651)

MetaR: simple, high-level languages for data analysis with the R ecosystem. Fabien Campagne, William ER Digan, Manuele Simi doi: [http://dx.doi.org/10.1101/030254](http://dx.doi.org/10.1101/030254)